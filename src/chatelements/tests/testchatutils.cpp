/*
 * Copyright (C) 2019 Simon Redman <simon@ergotech.com>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) version 3 of the
 *  License.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include <QtTest>

#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "Utils.h"

class ChatUtilsTest: public QObject
{
    Q_OBJECT

private:
    /**
     * Utils instance used by these tests
     * The class is basically static except that QML accesses it as a singleton
     * so that's what we'll do too
     */
    Utils utils;
    
    QRegularExpression linkMatcher;

    void testUrlHighlightHelper(const QString& url);

private Q_SLOTS:
  
    void init();

    void testSimpleUrlHighlight();
    void testLongUrlHighlight();
    void testSimpleLazyUrlHighlight();
    void testLongLazyUrlHighlight();
    void testNotAUrlHighlight();

    void testEmailHighlight();

};

void ChatUtilsTest::testUrlHighlightHelper(const QString& url)
{
    QString highlighted = utils.highlightLink(url);

    QRegularExpressionMatch match = linkMatcher.match(highlighted);

    QVERIFY2(match.hasMatch(), "The highlighted output is not formatted as an HTML link");
    // The first capture group should be the target of the URL
    QVERIFY2(match.captured(1).contains(url), "The highlighted output does not point to the proper URL");
}

void ChatUtilsTest::init()
{
    linkMatcher = QRegularExpression(QStringLiteral("<a href=(.*)>(.*)<\\/a>"));
}

/**
 * A well-formatted, simple URL should get highlighted
 */
void ChatUtilsTest::testSimpleUrlHighlight()
{
    QString url = QStringLiteral("https://kde.org");

    testUrlHighlightHelper(url);
}

void ChatUtilsTest::testLongUrlHighlight()
{
    QString url = QStringLiteral("https://api.kde.org/frameworks/kirigami/html/index.html");
  
    testUrlHighlightHelper(url);
}

/**
 * Most users won't type the full URL with https:// and so on. Try to match anyway.
 */
void ChatUtilsTest::testSimpleLazyUrlHighlight()
{
    QString url = QStringLiteral("kde.org");

    testUrlHighlightHelper(url);
}

void ChatUtilsTest::testLongLazyUrlHighlight()
{
    QString url = QStringLiteral("example.com/stuff/things/path/whatever");

    testUrlHighlightHelper(url);
}

/**
 * Sometimes we might mess up typing and forget a space. That's okay
 * (As long as the following word is long-ish because my URL match in Utils
 * is kind of lazy)
 */
void ChatUtilsTest::testNotAUrlHighlight()
{
    QString notUrl = QStringLiteral("end.Start");
    
    QString highlight = utils.highlightLink(notUrl);

    QVERIFY2(!linkMatcher.match(highlight).hasMatch(), "Found a match where we shouldn't have");
}

/**
 * We should be able to identify and highlight email addresses too!
 */
void ChatUtilsTest::testEmailHighlight()
{
    QString email = QStringLiteral("example@example.com");

    testUrlHighlightHelper(email);

    QString highlighted = utils.highlightLink(email);

    QRegularExpressionMatch match = linkMatcher.match(highlighted);

    QVERIFY2(match.captured(1).contains(QStringLiteral("mailto")),
             "Highlighted an email address but not as a mailto link");
}

QTEST_APPLESS_MAIN(ChatUtilsTest)
#include "testchatutils.moc"
