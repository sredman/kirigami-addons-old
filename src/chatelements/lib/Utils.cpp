/*
 *  Kaidan - A user-friendly XMPP client for every device!
 *
 *  Copyright (C) 2016-2019 Kaidan developers and contributors
 *  (see the LICENSE file for a full list of copyright authors)
 *
 *  Kaidan is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  In addition, as a special exception, the author of Kaidan gives
 *  permission to link the code of its release with the OpenSSL
 *  project's "OpenSSL" library (or with modified versions of it that
 *  use the same license as the "OpenSSL" library), and distribute the
 *  linked executables. You must obey the GNU General Public License in
 *  all respects for all of the code used other than "OpenSSL". If you
 *  modify this file, you may extend this exception to your version of
 *  the file, but you are not obligated to do so.  If you do not wish to
 *  do so, delete this exception statement from your version.
 *
 *  Kaidan is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Kaidan.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QClipboard>
#include <QGuiApplication>
#include <QRegularExpression>

#include "Utils.h"

QString Utils::formatMessage(const QString& message) const
{
    const QStringList words(message.toHtmlEscaped().split(QStringLiteral(" ")));

    QStringList processedWords;

    for (const auto& word : words) {
        QString processedWord = highlightLink(word);
        processedWords.append(processedWord);
    }
    return processedWords.join(QStringLiteral(" "));
}

QString Utils::highlightLink(const QString& word) const
{
    // Anything which starts with a valid URI should be highlighted
    // URI Spec: https://tools.ietf.org/html/rfc3986#section-3.1
    static QRegularExpression uriRegex(QStringLiteral("^[[:alpha:]]+[[:alpha:][:digit:]\\+\\-\\.]*\\:\\/\\/[^\\s]+"));
    // QUrl does not create mailto:// links from user input, so handle that ourselves
    // Email address spec: https://tools.ietf.org/html/rfc2822#section-3.4.1
    static QRegularExpression emailRegex(QStringLiteral(".*\\@.*\\.[[:alpha:]]{2,4}(?:$|\\s)"));
    // Since most users won't type the URI on a message, we need some other way to match link-shaped things
    // For lack of a better idea, I say anything which contains a block of a period, then two to four letters,
    // then an optional forward slash. If there is a forward slash, the link might continue, otherwise not
    // Anything of this form should be shoveled in to QUrl for processing into an actually proper URL
    static QRegularExpression fuzzyLinkRegex(QStringLiteral(".*\\.[[:alpha:]]{2,4}(?:\\/[^\\s]*|\\/?)(?:\\s|$)"));
    if (uriRegex.match(word).hasMatch()) {
        return QStringLiteral("<a href='%1'>%1</a>").arg(word);
    }
    if (emailRegex.match(word).hasMatch()) {
        // I think the emailRegex is strong enough that we can just slap mailto: on the front and call it good
        return QStringLiteral("<a href='mailto:%1'>%1</a>").arg(word);
    }
    if (fuzzyLinkRegex.match(word).hasMatch()) {
      QUrl url = QUrl::fromUserInput(word);
      if (url.isValid()) {
        return QStringLiteral("<a href='%2'>%1</a>").arg(word, url.toString());
      }
      // The URL might still be invalid if it had disallowed characters, in which case just don't match it
    }
    return word;
}

void Utils::copyToClipboard(const QString& text) const
{
    QGuiApplication::clipboard()->setText(text);
}

QObject* Utils::singletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine);
    Q_UNUSED(scriptEngine);

    return new Utils();
}
