#include <QQmlExtensionPlugin>
#include <QQmlEngine>
#include <QDebug>

#include "Enums.h"
#include "Utils.h"

class KirigamiAddonsChatPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    KirigamiAddonsChatPlugin() = default;
    ~KirigamiAddonsChatPlugin() = default;
    void initializeEngine(QQmlEngine *engine, const char *uri) override {};
    void registerTypes(const char *uri) override;
};

void KirigamiAddonsChatPlugin::registerTypes(const char *uri)
{
  qmlRegisterUncreatableMetaObject(Enums::staticMetaObject, uri, 0, 1, QStringLiteral("Enums").toLatin1().data(), QStringLiteral("Can't create object; only enums defined!"));
  qmlRegisterSingletonType<Utils>(uri, 0, 1, QStringLiteral("Utils").toLatin1().data(), Utils::singletonProvider);
}

#include "plugin.moc"
