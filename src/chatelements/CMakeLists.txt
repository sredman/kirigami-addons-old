set(chat_SRCS
    lib/Enums.h
    lib/Utils.cpp
    lib/plugin.cpp)

add_definitions(-DTRANSLATION_DOMAIN=\"kirigami_chat\")

if (BUILD_TESTING)
    add_subdirectory(tests)
endif()

add_library(chatplugin SHARED ${chat_SRCS})
target_link_libraries(chatplugin
        Qt5::Quick
        Qt5::Qml
        KF5::I18n
)

install(TARGETS chatplugin DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/kirigamiaddons/chat)
install(FILES
        qmldir
        ChatMessage.qml
        ChatMessageImage.qml
        Avatar.qml
        TextAvatar.qml
        RoundImage.qml
        DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/kirigamiaddons/chat)
